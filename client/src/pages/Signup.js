import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { signup } from "../store/authSlice";
import { Navigate } from "react-router-dom";

function Signup() {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const user = useSelector((state) => state.auth.user);
  const error = useSelector((state) => state.auth.error);
  const dispatch = useDispatch();

  const submitHandler = (event) => {
    event.preventDefault();
    dispatch(signup({ username, password })).then(() => {
      setUsername("");
      setPassword("");
    });
  };

  return (
    <div style={{ paddingBottom: '65px' }} className="flex justify-center items-center h-screen bg-gray-100">
      <header>
        <form
          onSubmit={submitHandler}
          className="bg-white shadow-md rounded-lg px-8 pt-6 pb-8 mb-4 w-96"
        >
          <div className="mb-4">
            <label htmlFor="username" className="block text-sm font-bold mb-2">
              Username
            </label>
            <input
              id="username"
              type="text"
              placeholder="Username"
              value={username}
              onChange={(e) => setUsername(e.target.value)}
              className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
            />
          </div>
          <div className="mb-6">
            <label htmlFor="password" className="block text-sm font-bold mb-2">
              Password
            </label>
            <input
              id="password"
              type="password"
              placeholder="Password"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
              className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
            />
          </div>
          <div className="flex items-center">
            <button
              type="submit"
              className="bg-green-600 hover:bg-green-700 text-white font-bold py-1 px-10 rounded-s focus:outline-none focus:shadow-outline"
            >
              Signup
            </button>
            <div className="w-full pl-2">
              {error && <p className="text-red-500 text-xs italic">{error}</p>}
              {user && <Navigate to="/" replace />}
            </div>
          </div>
        </form>
      </header>
    </div>
  );
}

export default Signup;
