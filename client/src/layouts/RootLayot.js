import Nav from '../components/Nav';
import BottomNav from '../components/BottomNav';
import {Outlet} from 'react-router-dom';
import { useSelector } from 'react-redux';





export default function RootLayout() {
    const user = useSelector((state) => state.auth.user);
    return (

        <div>
            {user ? (
                <>
                <Nav />
                <Outlet />
                <BottomNav />
                </>
            ) : (
                <>
                 <Outlet />
                </>
            ) }
            
        </div>
    )
}